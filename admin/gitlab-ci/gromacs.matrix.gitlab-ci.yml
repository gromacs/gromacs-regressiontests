# Build, test, and install variously configured GROMACS installations.
#
# This file contains job templates. Actual job configurations are inside gromacs.matrix/ directory.

.gromacs:base:configure:
  stage: configure-build
  cache: {}
  variables:
    KUBERNETES_CPU_LIMIT: 1
    KUBERNETES_CPU_REQUEST: 1
    KUBERNETES_MEMORY_REQUEST: 2Gi
    CMAKE_COMPILER_SCRIPT: ""
    CMAKE_EXTRA_OPTIONS: ""
    CMAKE_SIMD_OPTIONS: "-DGMX_SIMD=AVX2_256"
    CMAKE_MPI_OPTIONS: "-DGMX_THREAD_MPI=ON"
    CMAKE_PRECISION_OPTIONS: "-DGMX_DOUBLE=OFF"
    CMAKE_BUILD_TYPE_OPTIONS: "-DCMAKE_BUILD_TYPE=Debug"
    CMAKE_GPU_OPTIONS: "-DGMX_GPU=OFF"
  script:
    - bash -x admin/ci-scripts/gromacs-base-configure.sh
  artifacts:
    when: always
    paths:
      - $BUILD_DIR

.gromacs:base:build:
  stage: build
  script:
    - bash -x admin/ci-scripts/gromacs-base-build.sh
  artifacts:
    when: always
    paths:
      - $BUILD_DIR
      - $INSTALL_DIR/

.gromacs:base:regressiontest:
  variables:
    KUBERNETES_CPU_LIMIT: 4
    KUBERNETES_CPU_REQUEST: 4
    KUBERNETES_MEMORY_REQUEST: 4Gi
    REGRESSIONTEST_TOTAL_RANK_NUMBER: 4
    REGRESSIONTEST_OMP_RANK_NUMBER: 2
    REGRESSIONTEST_PME_RANK_NUMBER: 1
    REGRESSIONTEST_MPI_RUN_COMMAND: ""
    REGRESSIONTEST_DOUBLE: ""
    REGRESSIONTEST_PARALLEL: "-nt"
  stage: test
  cache: {}
  retry:
    max: 1
  script:
    - export LSAN_OPTIONS="suppressions=$CI_PROJECT_DIR/admin/lsan-suppressions.txt:print_suppressions=0"
    - export ASAN_OPTIONS="check_initialization_order=1:detect_invalid_pointer_pairs=1:strict_init_order=true:strict_string_checks=true:detect_stack_use_after_return=true"
    # Needed to run MPI enabled code in the docker images, until we set up different users
    - export OMPI_ALLOW_RUN_AS_ROOT=1
    - export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
    - source $INSTALL_DIR/bin/GMXRC
    - perl gmxtest.pl $REGRESSIONTEST_PARALLEL $REGRESSIONTEST_TOTAL_RANK_NUMBER -ntomp $REGRESSIONTEST_OMP_RANK_NUMBER -npme $REGRESSIONTEST_PME_RANK_NUMBER $REGRESSIONTEST_DOUBLE $REGRESSIONTEST_MPI_RUN_COMMAND -xml all
  artifacts:
    paths:
      - gmxtest.xml
    when: always
    expire_in: 1 week

include:
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.clang-14.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.clang-19-mpi.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.clang-ASAN.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.clang-TSAN.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.gcc-12.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.gcc-12-cuda-12.1.0.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.gcc-13-cuda-12.5.1.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.acpp-23.10.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.oneapi-2024.0-opencl.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.oneapi-2024.0-sycl.gitlab-ci.yml'
  - local: '/admin/gitlab-ci/gromacs.matrix/gromacs.gcc-11-rocm-6.2.2.gitlab-ci.yml'
