# Test goal: build with AdaptiveCpp/hipSYCL (ROCm backend) to check SYCL code compatibility
# Test intents (should change rarely and conservatively):
#   OS: Ubuntu newest supported
#   Compiler: AMD Clang
#   GPU: AdaptiveCpp
#   Scope: configure, build
# Test implementation choices (free to change as needed):
#   OS: Ubuntu 22.04
#   Build type: RelWithAssert
#   Compiler: AMD Clang 16 from ROCm 5.7.1
#   MPI: threadMPI (AMD build)
#   SIMD: AVX2_128

gromacs:acpp-rocm:configure:
  extends:
   - .gromacs:base:configure
   - .use-amdclang:base
   - .use-sycl
  rules:
   - !reference [.rules:merge-requests, rules]
  image: ${CI_REGISTRY}/gromacs/gromacs/ci-ubuntu-22.04-gcc-11-adaptivecpp-23.10.0-rocm-5.7.1
  variables:
    CMAKE: /usr/local/cmake-3.29.8/bin/cmake
    CMAKE_SIMD_OPTIONS: "-DGMX_SIMD=AVX2_128"
    CMAKE_BUILD_TYPE_OPTIONS: "-DCMAKE_BUILD_TYPE=RelWithAssert"
    CMAKE_GPU_OPTIONS: "-DGMX_GPU=SYCL -DGMX_SYCL=ACPP -DACPP_TARGETS=hip:gfx906,gfx1034 -DGMX_GPU_FFT_LIBRARY=vkFFT"
    # Unset COMPILER_LAUNCHER (previously set to ccache) because it conflicts with hipSYCL's syclcc-launcher
    CMAKE_EXTRA_OPTIONS: "-DCMAKE_C_COMPILER_LAUNCHER= -DCMAKE_CXX_COMPILER_LAUNCHER= -DGMX_INSTALL_LEGACY_API=ON"

gromacs:acpp-rocm:build:
  extends:
    - .gromacs:base:build
    - .before_script:default
    # Not using ccache because it plays poorly with syclcc-launcher
  rules:
    - !reference [.rules:merge-requests, rules]
  image: ${CI_REGISTRY}/gromacs/gromacs/ci-ubuntu-22.04-gcc-11-adaptivecpp-23.10.0-rocm-5.7.1
  variables:
    CMAKE: /usr/local/cmake-3.29.8/bin/cmake
  needs:
    - job: gromacs:acpp-rocm:configure
    - job: gromacs:prepare

gromacs:acpp-rocm:regressiontest:
  extends:
    - .gromacs:base:regressiontest
  rules:
    - !reference [.rules:skip-if-single-amd-gpu-unavailable, rules]
    - !reference [.rules:merge-requests-allow-failure, rules]
  image: ${CI_REGISTRY}/gromacs/gromacs/ci-ubuntu-22.04-gcc-11-adaptivecpp-23.10.0-rocm-5.7.1
  variables:
    CMAKE: /usr/local/cmake-3.29.8/bin/cmake
    GPU_VENDOR: "AMD"
    GPU_COUNT: 1
    REGRESSIONTEST_TOTAL_RANK_NUMBER: 2
    REGRESSIONTEST_OMP_RANK_NUMBER: 1
  tags:
    - $GITLAB_RUNNER_TAG_1X_AMD_GPU
  needs:
    - job: gromacs:acpp-rocm:build
    - job: gromacs:prepare
