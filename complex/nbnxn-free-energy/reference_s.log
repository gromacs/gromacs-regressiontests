        :-) GROMACS - gmx mdrun, 2022-beta1-dev-20211123-c831d37c81 (-:

                            GROMACS is written by:
     Andrey Alekseenko              Emile Apol              Rossen Apostolov     
         Paul Bauer           Herman J.C. Berendsen           Par Bjelkmar       
       Christian Blau           Viacheslav Bolnykh             Kevin Boyd        
     Aldert van Buuren           Rudi van Drunen             Anton Feenstra      
      Oliver Fleetwood             Gaurav Garg            Gilles Gouaillardet    
         Alan Gray               Gerrit Groenhof             Anca Hamuraru       
     Vincent Hindriksen          M. Eric Irrgang            Aleksei Iupinov      
     Christoph Junghans             Joe Jordan            Dimitrios Karkoulis    
        Peter Kasson                Jiri Kraus              Carsten Kutzner      
        Per Larsson              Justin A. Lemkul            Viveca Lindahl      
      Magnus Lundborg             Erik Marklund               Pascal Merz        
     Pieter Meulenhoff            Teemu Murtola               Szilard Pall       
        Sander Pronk              Roland Schulz              Michael Shirts      
      Alexey Shvetsov             Alfons Sijbers             Peter Tieleman      
        Jon Vincent              Teemu Virolainen          Christian Wennberg    
        Maarten Wolf              Artem Zhmurov       
                           and the project leaders:
        Mark Abraham, Berk Hess, Erik Lindahl, and David van der Spoel

Copyright (c) 1991-2000, University of Groningen, The Netherlands.
Copyright (c) 2001-2019, The GROMACS development team at
Uppsala University, Stockholm University and
the Royal Institute of Technology, Sweden.
check out http://www.gromacs.org for more information.

GROMACS is free software; you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License
as published by the Free Software Foundation; either version 2.1
of the License, or (at your option) any later version.

GROMACS:      gmx mdrun, version 2022-beta1-dev-20211123-c831d37c81
Executable:   /home/mabraham/git/review/build-cmake-gcc-reference/install/bin/gmx
Data prefix:  /home/mabraham/git/review/build-cmake-gcc-reference/install
Working dir:  /home/mabraham/git/regressiontests/complex/nbnxn-free-energy
Process ID:   249076
Command line:
  gmx mdrun -notunepme

GROMACS version:    2022-beta1-dev-20211123-c831d37c81
GIT SHA1 hash:      c831d37c8148f0af60cbc03ed7e35b7f5a20a9ae
Precision:          mixed
Memory model:       64 bit
MPI library:        none
OpenMP support:     disabled
GPU support:        disabled
SIMD instructions:  NONE
CPU FFT library:    fftpack (built-in)
GPU FFT library:    none
RDTSCP usage:       enabled
TNG support:        enabled
Hwloc support:      disabled
Tracing support:    disabled
C compiler:         /usr/bin/gcc-7 GNU 7.5.0
C compiler flags:   -pthread -Wno-unknown-pragmas -Wall -Wno-unused -Wunused-value -Wunused-parameter -Wextra -Wno-sign-compare -Wpointer-arith -Wundef -Wno-missing-field-initializers -O0 -g
C++ compiler:       /usr/bin/g++-7 GNU 7.5.0
C++ compiler flags: -pthread -Wno-unknown-pragmas -Wall -Wextra -Wpointer-arith -Wmissing-declarations -Wundef -Wno-missing-field-initializers -O0 -g


Running on 1 node with total 6 cores, 12 logical cores
Hardware detected:
  CPU info:
    Vendor: Intel
    Brand:  Intel(R) Core(TM) i7-10750H CPU @ 2.60GHz
    Family: 6   Model: 165   Stepping: 2
    Features: aes apic avx avx2 clfsh cmov cx8 cx16 f16c fma htt intel lahf mmx msr nonstop_tsc pcid pclmuldq pdcm pdpe1gb popcnt pse rdrnd rdtscp sse2 sse3 sse4.1 sse4.2 ssse3 tdt x2apic
  Hardware topology: Basic
    Sockets, cores, and logical processors:
      Socket  0: [   0   6] [   1   7] [   2   8] [   3   9] [   4  10] [   5  11]

Highest SIMD level supported by all nodes in run: AVX2_256
SIMD instructions selected at compile time:       None
This program was compiled for different hardware than you are running on,
which could influence performance.

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
M. J. Abraham, T. Murtola, R. Schulz, S. Páll, J. C. Smith, B. Hess, E.
Lindahl
GROMACS: High performance molecular simulations through multi-level
parallelism from laptops to supercomputers
SoftwareX 1 (2015) pp. 19-25
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
S. Páll, M. J. Abraham, C. Kutzner, B. Hess, E. Lindahl
Tackling Exascale Software Challenges in Molecular Dynamics Simulations with
GROMACS
In S. Markidis & E. Laure (Eds.), Solving Software Challenges for Exascale 8759 (2015) pp. 3-27
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
S. Pronk, S. Páll, R. Schulz, P. Larsson, P. Bjelkmar, R. Apostolov, M. R.
Shirts, J. C. Smith, P. M. Kasson, D. van der Spoel, B. Hess, and E. Lindahl
GROMACS 4.5: a high-throughput and highly parallel open source molecular
simulation toolkit
Bioinformatics 29 (2013) pp. 845-54
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
B. Hess and C. Kutzner and D. van der Spoel and E. Lindahl
GROMACS 4: Algorithms for highly efficient, load-balanced, and scalable
molecular simulation
J. Chem. Theory Comput. 4 (2008) pp. 435-447
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
D. van der Spoel, E. Lindahl, B. Hess, G. Groenhof, A. E. Mark and H. J. C.
Berendsen
GROMACS: Fast, Flexible and Free
J. Comp. Chem. 26 (2005) pp. 1701-1719
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
E. Lindahl and B. Hess and D. van der Spoel
GROMACS 3.0: A package for molecular simulation and trajectory analysis
J. Mol. Mod. 7 (2001) pp. 306-317
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
H. J. C. Berendsen, D. van der Spoel and R. van Drunen
GROMACS: A message-passing parallel molecular dynamics implementation
Comp. Phys. Comm. 91 (1995) pp. 43-56
-------- -------- --- Thank You --- -------- --------

Input Parameters:
   integrator                     = md
   tinit                          = 0
   dt                             = 0.0005
   nsteps                         = 20
   init-step                      = 0
   simulation-part                = 1
   mts                            = false
   comm-mode                      = Linear
   nstcomm                        = 4
   bd-fric                        = 0
   ld-seed                        = 975196
   emtol                          = 10
   emstep                         = 0.01
   niter                          = 20
   fcstep                         = 0
   nstcgsteep                     = 200
   nbfgscorr                      = 10
   rtpi                           = 0.05
   nstxout                        = 20
   nstvout                        = 20
   nstfout                        = 20
   nstlog                         = 4
   nstcalcenergy                  = 4
   nstenergy                      = 4
   nstxout-compressed             = 0
   compressed-x-precision         = 200
   cutoff-scheme                  = Verlet
   nstlist                        = 10
   pbc                            = xyz
   periodic-molecules             = false
   verlet-buffer-tolerance        = 0.005
   rlist                          = 0.9
   coulombtype                    = PME
   coulomb-modifier               = Potential-shift
   rcoulomb-switch                = 0
   rcoulomb                       = 0.9
   epsilon-r                      = 1
   epsilon-rf                     = 1
   vdw-type                       = Cut-off
   vdw-modifier                   = Potential-shift
   rvdw-switch                    = 0
   rvdw                           = 0.9
   DispCorr                       = EnerPres
   table-extension                = 1
   fourierspacing                 = 0
   fourier-nx                     = 20
   fourier-ny                     = 20
   fourier-nz                     = 20
   pme-order                      = 4
   ewald-rtol                     = 1e-05
   ewald-rtol-lj                  = 0.001
   lj-pme-comb-rule               = Geometric
   ewald-geometry                 = 3d
   epsilon-surface                = 0
   tcoupl                         = V-rescale
   nsttcouple                     = 10
   nh-chain-length                = 0
   print-nose-hoover-chain-variables = false
   pcoupl                         = Berendsen
   pcoupltype                     = Isotropic
   nstpcouple                     = 10
   tau-p                          = 1
   compressibility (3x3):
      compressibility[    0]={ 4.60000e-05,  0.00000e+00,  0.00000e+00}
      compressibility[    1]={ 0.00000e+00,  4.60000e-05,  0.00000e+00}
      compressibility[    2]={ 0.00000e+00,  0.00000e+00,  4.60000e-05}
   ref-p (3x3):
      ref-p[    0]={ 1.00000e+00,  0.00000e+00,  0.00000e+00}
      ref-p[    1]={ 0.00000e+00,  1.00000e+00,  0.00000e+00}
      ref-p[    2]={ 0.00000e+00,  0.00000e+00,  1.00000e+00}
   refcoord-scaling               = No
   posres-com (3):
      posres-com[0]= 0.00000e+00
      posres-com[1]= 0.00000e+00
      posres-com[2]= 0.00000e+00
   posres-comB (3):
      posres-comB[0]= 0.00000e+00
      posres-comB[1]= 0.00000e+00
      posres-comB[2]= 0.00000e+00
   QMMM                           = false
qm-opts:
   ngQM                           = 0
   constraint-algorithm           = Lincs
   continuation                   = false
   Shake-SOR                      = false
   shake-tol                      = 0.0001
   lincs-order                    = 4
   lincs-iter                     = 1
   lincs-warnangle                = 30
   nwall                          = 0
   wall-type                      = 9-3
   wall-r-linpot                  = -1
   wall-atomtype[0]               = -1
   wall-atomtype[1]               = -1
   wall-density[0]                = 0
   wall-density[1]                = 0
   wall-ewald-zfac                = 3
   pull                           = false
   awh                            = false
   rotation                       = false
   interactiveMD                  = false
   disre                          = No
   disre-weighting                = Conservative
   disre-mixed                    = false
   dr-fc                          = 200
   dr-tau                         = 0
   nstdisreout                    = 20
   orire-fc                       = 0
   orire-tau                      = 0
   nstorireout                    = 20
   free-energy                    = slow-growth
   init-lambda                    = 0
   init-lambda-state              = -1
   delta-lambda                   = 0.05
   nstdhdl                        = 100
   n-lambdas                      = 0
   calc-lambda-neighbors          = 1
   dhdl-print-energy              = no
   sc-alpha                       = 0.6
   sc-power                       = 1
   sc-r-power                     = 6
   sc-sigma                       = 0.28
   sc-sigma-min                   = 0.28
   sc-coul                        = true
   dh-hist-size                   = 0
   dh-hist-spacing                = 0.1
   separate-dhdl-file             = yes
   dhdl-derivatives               = yes
   sc-function                    = beutler
   sc-gapsys-scale-linpoint-lj    = 0.85
   sc-gapsys-scale-linpoint-q     = 0.3
   sc-gapsys-sigma-lj             = 0.3
   cos-acceleration               = 0
   deform (3x3):
      deform[    0]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      deform[    1]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      deform[    2]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
   simulated-tempering            = false
   swapcoords                     = no
   userint1                       = 0
   userint2                       = 0
   userint3                       = 0
   userint4                       = 0
   userreal1                      = 0
   userreal2                      = 0
   userreal3                      = 0
   userreal4                      = 0
   applied-forces:
     electric-field:
       x:
         E0                       = 0
         omega                    = 0
         t0                       = 0
         sigma                    = 0
       y:
         E0                       = 0
         omega                    = 0
         t0                       = 0
         sigma                    = 0
       z:
         E0                       = 0
         omega                    = 0
         t0                       = 0
         sigma                    = 0
     density-guided-simulation:
       active                     = false
       group                      = protein
       similarity-measure         = inner-product
       atom-spreading-weight      = unity
       force-constant             = 1e+09
       gaussian-transform-spreading-width = 0.2
       gaussian-transform-spreading-range-in-multiples-of-width = 4
       reference-density-filename = reference.mrc
       nst                        = 1
       normalize-densities        = true
       adaptive-force-scaling     = false
       adaptive-force-scaling-time-constant = 4
       shift-vector               = 
       transformation-matrix      = 
     qmmm-cp2k:
       active                     = false
       qmgroup                    = System
       qmmethod                   = PBE
       qmfilenames                = 
       qmcharge                   = 0
       qmmultiplicity             = 1
grpopts:
   nrdf:        3045
   ref-t:         298
   tau-t:         0.5
annealing:          No
annealing-npoints:           0
   acc:	           0           0           0
   nfreeze:           N           N           N
   energygrp-flags[  0]: 0

Changing nstlist from 10 to 100, rlist from 0.9 to 0.958


Initializing Domain Decomposition on 1 ranks
Dynamic load balancing: auto
Using update groups, nr 516, average size 2.9 atoms, max. radius 0.084 nm
Minimum cell size due to atom displacement: 0.421 nm
Initial maximum distances in bonded interactions:
    two-body bonded interactions: 0.375 nm, Exclusion, atoms 2 13
  multi-body bonded interactions: 0.286 nm, Improper Dih., atoms 9 11
Minimum cell size due to bonded interactions: 0.314 nm
Using 0 separate PME ranks because: there are too few total ranks for efficient splitting
Optimizing the DD grid for 1 cells with a minimum initial size of 0.421 nm
The maximum allowed number of cells is: X 5 Y 5 Z 5
Domain decomposition grid 1 x 1 x 1, separate PME ranks 0
PME domain decomposition: 1 x 1 x 1

The initial number of communication pulses is:
The initial domain decomposition cell size is:

The maximum allowed distance for atom groups involved in interactions is:
                 non-bonded interactions           1.126 nm
(the following are initial values, they could change due to box deformation)
            two-body bonded interactions  (-rdd)   1.126 nm
          multi-body bonded interactions  (-rdd)   1.126 nm

When dynamic load balancing gets turned on, these settings will change to:
The maximum number of communication pulses is:
The minimum size for domain decomposition cells is 1.126 nm
The requested allowed shrink of DD cells (option -dds) is: 0.80
The allowed shrink of domain decomposition cells is:
The maximum allowed distance for atom groups involved in interactions is:
                 non-bonded interactions           1.126 nm
            two-body bonded interactions  (-rdd)   1.126 nm
          multi-body bonded interactions  (-rdd)   1.126 nm

NOTE: GROMACS was compiled without OpenMP and (thread-)MPI support, can only use a single CPU core

Pinning threads with an auto-selected logical core stride of 2
System total charge, top. A: 0.000 top. B: 0.000
Will do PME sum in reciprocal space for electrostatic interactions.

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
U. Essmann, L. Perera, M. L. Berkowitz, T. Darden, H. Lee and L. G. Pedersen 
A smooth particle mesh Ewald method
J. Chem. Phys. 103 (1995) pp. 8577-8592
-------- -------- --- Thank You --- -------- --------

Using a Gaussian width (1/beta) of 0.288146 nm for Ewald
Potential shift: LJ r^-12: -3.541e+00 r^-6: -1.882e+00, Ewald -1.111e-05
Initialized non-bonded Ewald tables, spacing: 8.85e-04 size: 2214

Long Range LJ corr.: <C6> 2.9770e-04


Using plain C 4x4 nonbonded short-range kernels

WARNING: Using the slow plain C kernels. This should
not happen during routine usage on supported platforms.

Using a dual 4x4 pair-list setup updated with dynamic pruning:
  outer list: updated every 100 steps, buffer 0.058 nm, rlist 0.958 nm
  inner list: updated every  37 steps, buffer 0.001 nm, rlist 0.901 nm
At tolerance 0.005 kJ/mol/ps per atom, equivalent classical 1x1 list would be:
  outer list: updated every 100 steps, buffer 0.142 nm, rlist 1.042 nm
  inner list: updated every  37 steps, buffer 0.038 nm, rlist 0.938 nm

Using full Lennard-Jones parameter combination matrix
There are 16 atoms and 13 charges for free energy perturbation
Removing pbc first time

Linking all bonded interactions to atoms


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
S. Miyamoto and P. A. Kollman
SETTLE: An Analytical Version of the SHAKE and RATTLE Algorithms for Rigid
Water Models
J. Comp. Chem. 13 (1992) pp. 952-962
-------- -------- --- Thank You --- -------- --------

Initial vector of lambda components:[     0.0000     0.0000     0.0000     0.0000     0.0000     0.0000     0.0000 ]

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
G. Bussi, D. Donadio and M. Parrinello
Canonical sampling through velocity rescaling
J. Chem. Phys. 126 (2007) pp. 014101
-------- -------- --- Thank You --- -------- --------

There are: 1516 Atoms

Atom distribution over 1 domains: av 1516 stddev 0 min 1516 max 1516

Constraining the starting coordinates (step 0)

Constraining the coordinates at t0-dt (step 0)
Center of mass motion removal mode is Linear
We have the following groups for center of mass motion removal:
  0:  rest
RMS relative constraint deviation after constraining: 0.00e+00
Initial temperature: 291.771 K

Started mdrun on rank 0 Tue Nov 23 15:49:47 2021

           Step           Time
              0        0.00000

   Energies (kJ/mol)
        G96Bond       G96Angle  Improper Dih.        LJ (SR)  Disper. corr.
    1.34229e+01    1.35642e+01    5.09136e+00    4.52798e+03   -2.60670e+02
   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.   Total Energy
   -2.78690e+04    1.75573e+02   -2.33941e+04    3.69409e+03   -1.97000e+04
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)    dVremain/dl
   -1.96998e+04    2.91820e+02   -2.87945e+02   -2.83090e+02   -9.92086e+01

           Step           Time
              4        0.00200

   Energies (kJ/mol)
        G96Bond       G96Angle  Improper Dih.        LJ (SR)  Disper. corr.
    1.17252e+01    8.53806e+00    4.46263e+00    4.52668e+03   -2.58414e+02
   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.   Total Energy
   -2.78659e+04    1.80380e+02   -2.33925e+04    3.69546e+03   -1.96971e+04
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)    dVremain/dl
   -1.96991e+04    2.91929e+02   -2.85471e+02   -3.74975e+02    6.74275e+01

           Step           Time
              8        0.00400

   Energies (kJ/mol)
        G96Bond       G96Angle  Improper Dih.        LJ (SR)  Disper. corr.
    1.31350e+01    9.59870e+00    4.66516e+00    4.53225e+03   -2.56141e+02
   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.   Total Energy
   -2.78575e+04    1.85214e+02   -2.33687e+04    3.69199e+03   -1.96768e+04
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)    dVremain/dl
   -1.96788e+04    2.91654e+02   -2.82960e+02   -4.50375e+02    1.29829e+02

           Step           Time
             12        0.00600

   Energies (kJ/mol)
        G96Bond       G96Angle  Improper Dih.        LJ (SR)  Disper. corr.
    2.16534e+01    1.40236e+01    5.16525e+00    4.54461e+03   -2.53897e+02
   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.   Total Energy
   -2.78569e+04    1.89799e+02   -2.33355e+04    3.69675e+03   -1.96388e+04
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)    dVremain/dl
   -1.96484e+04    2.92031e+02   -2.80515e+02   -1.10656e+03    1.70486e+02

           Step           Time
             16        0.00800

   Energies (kJ/mol)
        G96Bond       G96Angle  Improper Dih.        LJ (SR)  Disper. corr.
    2.88708e+01    1.68965e+01    5.70173e+00    4.55415e+03   -2.51624e+02
   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.   Total Energy
   -2.78564e+04    1.93934e+02   -2.33085e+04    3.70715e+03   -1.96014e+04
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)    dVremain/dl
   -1.96110e+04    2.92852e+02   -2.78003e+02   -6.32330e+02    2.01934e+02

           Step           Time
             20        0.01000

Writing checkpoint, step 20 at Tue Nov 23 15:49:48 2021


   Energies (kJ/mol)
        G96Bond       G96Angle  Improper Dih.        LJ (SR)  Disper. corr.
    2.23845e+01    1.72384e+01    6.20234e+00    4.56397e+03   -2.49351e+02
   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.   Total Energy
   -2.78577e+04    1.97398e+02   -2.32998e+04    3.74130e+03   -1.95585e+04
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)    dVremain/dl
   -1.95678e+04    2.95550e+02   -2.75491e+02   -5.68309e+02    2.27987e+02


Energy conservation over simulation part #1 of length 0.01 ns, time 0 to 0.01 ns
  Conserved energy drift: 8.71e+00 kJ/mol/ps per atom


	<======  ###############  ==>
	<====  A V E R A G E S  ====>
	<==  ###############  ======>

	Statistics over 21 steps using 6 frames

   Energies (kJ/mol)
        G96Bond       G96Angle  Improper Dih.        LJ (SR)  Disper. corr.
    1.85320e+01    1.33099e+01    5.21475e+00    4.54161e+03   -2.55016e+02
   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.   Total Energy
   -2.78606e+04    1.87050e+02   -2.33499e+04    3.70446e+03   -1.96454e+04
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)    dVremain/dl
   -1.96508e+04    2.92639e+02   -2.81731e+02   -5.69274e+02    1.16409e+02

          Box-X          Box-Y          Box-Z
    2.46924e+00    2.46924e+00    2.46924e+00

   Total Virial (kJ/mol)
    1.67454e+03    6.77230e+01   -4.30506e+02
    6.64559e+01    1.61024e+03   -2.44386e+02
   -4.30838e+02   -2.43874e+02    1.19386e+03

   Pressure (bar)
   -1.01855e+03   -2.68526e+02    9.70900e+02
   -2.65731e+02   -7.35597e+02    4.42652e+02
    9.71632e+02    4.41523e+02    4.63243e+01


	M E G A - F L O P S   A C C O U N T I N G

 NB=Group-cutoff nonbonded kernels    NxN=N-by-N cluster Verlet kernels
 RF=Reaction-Field  VdW=Van der Waals  QSTab=quadratic-spline table
 W3=SPC/TIP3p  W4=TIP4p (single or pairs)
 V&F=Potential and force  V=Potential only  F=Force only

 Computing:                               M-Number         M-Flops  % Flops
-----------------------------------------------------------------------------
 NB Free energy kernel                   35.616672          35.617     5.8
 Pair Search distance check               0.387218           3.485     0.6
 NxN QSTab Elec. + LJ [F]                 4.088640         167.634    27.1
 NxN QSTab Elec. + LJ [V&F]               1.635456          96.492    15.6
 NxN QSTab Elec. [F]                      4.007280         136.248    22.0
 NxN QSTab Elec. [V&F]                    1.602912          65.719    10.6
 Calc Weights                             0.095508           3.438     0.6
 Spread Q Bspline                         4.075008           8.150     1.3
 Gather F Bspline                         4.075008          24.450     4.0
 3D-FFT                                   8.712984          69.704    11.3
 Solve PME                                0.016800           1.075     0.2
 Reset In Box                             0.001516           0.005     0.0
 CG-CoM                                   0.003032           0.009     0.0
 Bonds                                    0.000357           0.021     0.0
 Angles                                   0.000567           0.095     0.0
 Impropers                                0.000420           0.087     0.0
 Virial                                   0.010927           0.197     0.0
 Stop-CM                                  0.010612           0.106     0.0
 P-Coupling                               0.004548           0.027     0.0
 Calc-Ekin                                0.033352           0.901     0.1
 Constraint-V                             0.033000           0.297     0.0
 Constraint-Vir                           0.010500           0.252     0.0
 Settle                                   0.011500           4.255     0.7
-----------------------------------------------------------------------------
 Total                                                     618.264   100.0
-----------------------------------------------------------------------------


    D O M A I N   D E C O M P O S I T I O N   S T A T I S T I C S

 av. #atoms communicated per step for force:  2 x 0.0


     R E A L   C Y C L E   A N D   T I M E   A C C O U N T I N G

On 1 MPI rank

 Computing:          Num   Num      Call    Wall time         Giga-Cycles
                     Ranks Threads  Count      (s)         total sum    %
-----------------------------------------------------------------------------
 Domain decomp.         1    1          1       0.004          0.011   0.5
 Neighbor search        1    1          1       0.022          0.058   2.6
 Force                  1    1         21       0.680          1.762  79.4
 PME mesh               1    1         21       0.112          0.290  13.1
 NB X/F buffer ops.     1    1         41       0.001          0.003   0.1
 Write traj.            1    1          2       0.015          0.040   1.8
 Update                 1    1         21       0.003          0.008   0.3
 Constraints            1    1         23       0.005          0.013   0.6
 Rest                                           0.014          0.035   1.6
-----------------------------------------------------------------------------
 Total                                          0.857          2.220 100.0
-----------------------------------------------------------------------------
 Breakdown of PME mesh computation
-----------------------------------------------------------------------------
 PME spread             1    1         42       0.024          0.062   2.8
 PME gather             1    1         42       0.038          0.097   4.4
 PME 3D-FFT             1    1         84       0.028          0.072   3.2
 PME solve Elec         1    1         42       0.022          0.058   2.6
-----------------------------------------------------------------------------

               Core t (s)   Wall t (s)        (%)
       Time:        0.856        0.857      100.0
                 (ns/day)    (hour/ns)
Performance:        1.059       22.659
Finished mdrun on rank 0 Tue Nov 23 15:49:48 2021

