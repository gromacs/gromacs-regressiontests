           :-) GROMACS - gmx mdrun, 2023-dev-20220615-b6691e587c (-:

Copyright 1991-2023 The GROMACS Authors.
GROMACS is free software; you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License
as published by the Free Software Foundation; either version 2.1
of the License, or (at your option) any later version.

                         Current GROMACS contributors:
       Mark Abraham           Andrey Alekseenko           Cathrine Bergh      
      Christian Blau            Eliane Briand               Kevin Boyd        
     Oliver Fleetwood         Stefan Fleischmann           Vytas Gapsys       
       Gaurav Garg           Gilles Gouaillardet            Alan Gray         
      Victor Holanda           M. Eric Irrgang              Joe Jordan        
    Christoph Junghans        Prashanth Kanduri           Sebastian Kehl      
     Sebastian Keller          Carsten Kutzner           Magnus Lundborg      
       Pascal Merz              Dmitry Morozov             Szilard Pall       
      Roland Schulz             Michael Shirts         David van der Spoel    
     Alessandra Villa      Sebastian Wingbermuehle        Artem Zhmurov       

                         Previous GROMACS contributors:
        Emile Apol             Rossen Apostolov           James Barnett       
  Herman J.C. Berendsen          Par Bjelkmar           Viacheslav Bolnykh    
    Aldert van Buuren          Carlo Camilloni           Rudi van Drunen      
      Anton Feenstra           Gerrit Groenhof            Bert de Groot       
      Anca Hamuraru           Vincent Hindriksen         Aleksei Iupinov      
   Dimitrios Karkoulis           Peter Kasson               Jiri Kraus        
       Per Larsson             Justin A. Lemkul           Viveca Lindahl      
      Erik Marklund           Pieter Meulenhoff           Vedran Miletic      
      Teemu Murtola              Sander Pronk            Alexey Shvetsov      
      Alfons Sijbers            Peter Tieleman             Jon Vincent        
     Teemu Virolainen         Christian Wennberg           Maarten Wolf       

                  Coordinated by the GROMACS project leaders:
                    Paul Bauer, Berk Hess, and Erik Lindahl

GROMACS:      gmx mdrun, version 2023-dev-20220615-b6691e587c
Executable:   /home/hess/gmx_main/build/reference/bin/gmx
Data prefix:  /home/hess/gmx_main (source tree)
Working dir:  /data/hess/regressiontests/complex/swap_x
Process ID:   23296
Command line:
  gmx mdrun -deffnm reference_s

GROMACS version:    2023-dev-20220615-b6691e587c
GIT SHA1 hash:      b6691e587c6496ab836a0b66649ece915e368a9c
Precision:          mixed
Memory model:       64 bit
MPI library:        none
OpenMP support:     disabled
GPU support:        disabled
SIMD instructions:  NONE
CPU FFT library:    fftpack (built-in)
GPU FFT library:    none
RDTSCP usage:       enabled
TNG support:        enabled
Hwloc support:      disabled
Tracing support:    disabled
C compiler:         /usr/bin/cc GNU 7.5.0
C compiler flags:   -pthread -Wno-unknown-pragmas -Wall -Wno-unused -Wunused-value -Wunused-parameter -Wextra -Wno-sign-compare -Wpointer-arith -Wundef -Wno-missing-field-initializers -O0 -g
C++ compiler:       /usr/bin/c++ GNU 7.5.0
C++ compiler flags: -pthread -Wno-unknown-pragmas -Wall -Wextra -Wpointer-arith -Wmissing-declarations -Wundef -Wno-missing-field-initializers -O0 -g


Running on 1 node with total 6 cores, 12 processing units
Hardware detected on host tcbl10.scilifelab.se:
  CPU info:
    Vendor: Intel
    Brand:  Intel(R) Core(TM) i7-5820K CPU @ 3.30GHz
    Family: 6   Model: 63   Stepping: 2
    Features: aes apic avx avx2 clfsh cmov cx8 cx16 f16c fma htt intel lahf mmx msr nonstop_tsc pcid pclmuldq pdcm pdpe1gb popcnt pse rdrnd rdtscp sse2 sse3 sse4.1 sse4.2 ssse3 tdt x2apic
  Hardware topology: Basic
    Packages, cores, and logical processors:
    [indices refer to OS logical processors]
      Package  0: [   0   6] [   1   7] [   2   8] [   3   9] [   4  10] [   5  11]
    CPU limit set by OS: -1   Recommended max number of threads: 12

Highest SIMD level supported by all nodes in run: AVX2_256
SIMD instructions selected at compile time:       None
This program was compiled for different hardware than you are running on,
which could influence performance.

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
M. J. Abraham, T. Murtola, R. Schulz, S. Páll, J. C. Smith, B. Hess, E.
Lindahl
GROMACS: High performance molecular simulations through multi-level
parallelism from laptops to supercomputers
SoftwareX 1 (2015) pp. 19-25
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
S. Páll, M. J. Abraham, C. Kutzner, B. Hess, E. Lindahl
Tackling Exascale Software Challenges in Molecular Dynamics Simulations with
GROMACS
In S. Markidis & E. Laure (Eds.), Solving Software Challenges for Exascale 8759 (2015) pp. 3-27
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
S. Pronk, S. Páll, R. Schulz, P. Larsson, P. Bjelkmar, R. Apostolov, M. R.
Shirts, J. C. Smith, P. M. Kasson, D. van der Spoel, B. Hess, and E. Lindahl
GROMACS 4.5: a high-throughput and highly parallel open source molecular
simulation toolkit
Bioinformatics 29 (2013) pp. 845-54
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
B. Hess and C. Kutzner and D. van der Spoel and E. Lindahl
GROMACS 4: Algorithms for highly efficient, load-balanced, and scalable
molecular simulation
J. Chem. Theory Comput. 4 (2008) pp. 435-447
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
D. van der Spoel, E. Lindahl, B. Hess, G. Groenhof, A. E. Mark and H. J. C.
Berendsen
GROMACS: Fast, Flexible and Free
J. Comp. Chem. 26 (2005) pp. 1701-1719
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
E. Lindahl and B. Hess and D. van der Spoel
GROMACS 3.0: A package for molecular simulation and trajectory analysis
J. Mol. Mod. 7 (2001) pp. 306-317
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
H. J. C. Berendsen, D. van der Spoel and R. van Drunen
GROMACS: A message-passing parallel molecular dynamics implementation
Comp. Phys. Comm. 91 (1995) pp. 43-56
-------- -------- --- Thank You --- -------- --------

Input Parameters:
   integrator                     = md
   tinit                          = 0
   dt                             = 0.004
   nsteps                         = 2
   init-step                      = 0
   simulation-part                = 1
   mts                            = false
   comm-mode                      = Linear
   nstcomm                        = 10
   bd-fric                        = 0
   ld-seed                        = 1993
   emtol                          = 10
   emstep                         = 0.01
   niter                          = 20
   fcstep                         = 0
   nstcgsteep                     = 1000
   nbfgscorr                      = 10
   rtpi                           = 0.05
   nstxout                        = 1
   nstvout                        = 1
   nstfout                        = 1
   nstlog                         = 1
   nstcalcenergy                  = 1
   nstenergy                      = 1
   nstxout-compressed             = 1
   compressed-x-precision         = 1000
   cutoff-scheme                  = Verlet
   nstlist                        = 10
   pbc                            = xyz
   periodic-molecules             = false
   verlet-buffer-tolerance        = 0.005
   rlist                          = 1.014
   coulombtype                    = PME
   coulomb-modifier               = Potential-shift
   rcoulomb-switch                = 1
   rcoulomb                       = 1
   epsilon-r                      = 1
   epsilon-rf                     = 1
   vdw-type                       = Cut-off
   vdw-modifier                   = Potential-shift
   rvdw-switch                    = 1
   rvdw                           = 1
   DispCorr                       = Ener
   table-extension                = 1
   fourierspacing                 = 0.135
   fourier-nx                     = 72
   fourier-ny                     = 32
   fourier-nz                     = 32
   pme-order                      = 4
   ewald-rtol                     = 1e-05
   ewald-rtol-lj                  = 0.001
   lj-pme-comb-rule               = Geometric
   ewald-geometry                 = 3d
   epsilon-surface                = 0
   tcoupl                         = Berendsen
   nsttcouple                     = 1
   nh-chain-length                = 0
   print-nose-hoover-chain-variables = false
   pcoupl                         = Berendsen
   pcoupltype                     = Semiisotropic
   nstpcouple                     = 10
   tau-p                          = 2
   compressibility (3x3):
      compressibility[    0]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      compressibility[    1]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      compressibility[    2]={ 0.00000e+00,  0.00000e+00,  4.50000e-05}
   ref-p (3x3):
      ref-p[    0]={ 1.00000e+00,  0.00000e+00,  0.00000e+00}
      ref-p[    1]={ 0.00000e+00,  1.00000e+00,  0.00000e+00}
      ref-p[    2]={ 0.00000e+00,  0.00000e+00,  1.00000e+00}
   refcoord-scaling               = COM
   posres-com (3):
      posres-com[0]= 5.02088e-01
      posres-com[1]= 5.06393e-01
      posres-com[2]= 4.85252e-01
   posres-comB (3):
      posres-comB[0]= 5.02088e-01
      posres-comB[1]= 5.06393e-01
      posres-comB[2]= 4.85252e-01
   QMMM                           = false
qm-opts:
   ngQM                           = 0
   constraint-algorithm           = Lincs
   continuation                   = false
   Shake-SOR                      = false
   shake-tol                      = 0.0001
   lincs-order                    = 2
   lincs-iter                     = 1
   lincs-warnangle                = 30
   nwall                          = 0
   wall-type                      = 9-3
   wall-r-linpot                  = -1
   wall-atomtype[0]               = -1
   wall-atomtype[1]               = -1
   wall-density[0]                = 0
   wall-density[1]                = 0
   wall-ewald-zfac                = 3
   pull                           = false
   awh                            = false
   rotation                       = false
   interactiveMD                  = false
   disre                          = No
   disre-weighting                = Conservative
   disre-mixed                    = false
   dr-fc                          = 1000
   dr-tau                         = 0
   nstdisreout                    = 100
   orire-fc                       = 0
   orire-tau                      = 0
   nstorireout                    = 100
   free-energy                    = no
   cos-acceleration               = 0
   deform (3x3):
      deform[    0]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      deform[    1]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      deform[    2]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
   simulated-tempering            = false
   swapcoords                     = X
   swap-frequency                 = 1
   massw_split0                   = true
   split atoms group 0 (83):
      split atoms group 0[0]=0
      split atoms group 0[1]=2
      split atoms group 0[2]=4
      split atoms group 0[3]=6
      split atoms group 0[4]=8
      split atoms group 0[5]=10
      split atoms group 0[6]=12
      split atoms group 0[7]=14
      split atoms group 0[8]=16
      split atoms group 0[9]=18
      split atoms group 0[10]=20
      split atoms group 0[11]=22
      split atoms group 0[12]=24
      split atoms group 0[13]=26
      split atoms group 0[14]=28
      split atoms group 0[15]=30
      split atoms group 0[16]=32
      split atoms group 0[17]=34
      split atoms group 0[18]=36
      split atoms group 0[19]=38
      split atoms group 0[20]=40
      split atoms group 0[21]=42
      split atoms group 0[22]=44
      split atoms group 0[23]=46
      split atoms group 0[24]=48
      split atoms group 0[25]=50
      split atoms group 0[26]=52
      split atoms group 0[27]=54
      split atoms group 0[28]=56
      split atoms group 0[29]=58
      split atoms group 0[30]=60
      split atoms group 0[31]=62
      split atoms group 0[32]=64
      split atoms group 0[33]=66
      split atoms group 0[34]=68
      split atoms group 0[35]=70
      split atoms group 0[36]=72
      split atoms group 0[37]=74
      split atoms group 0[38]=76
      split atoms group 0[39]=78
      split atoms group 0[40]=80
      split atoms group 0[41]=82
      split atoms group 0[42]=84
      split atoms group 0[43]=86
      split atoms group 0[44]=88
      split atoms group 0[45]=90
      split atoms group 0[46]=92
      split atoms group 0[47]=94
      split atoms group 0[48]=96
      split atoms group 0[49]=98
      split atoms group 0[50]=100
      split atoms group 0[51]=102
      split atoms group 0[52]=104
      split atoms group 0[53]=106
      split atoms group 0[54]=108
      split atoms group 0[55]=110
      split atoms group 0[56]=112
      split atoms group 0[57]=114
      split atoms group 0[58]=116
      split atoms group 0[59]=118
      split atoms group 0[60]=120
      split atoms group 0[61]=122
      split atoms group 0[62]=124
      split atoms group 0[63]=126
      split atoms group 0[64]=128
      split atoms group 0[65]=130
      split atoms group 0[66]=132
      split atoms group 0[67]=134
      split atoms group 0[68]=136
      split atoms group 0[69]=138
      split atoms group 0[70]=140
      split atoms group 0[71]=142
      split atoms group 0[72]=144
      split atoms group 0[73]=146
      split atoms group 0[74]=148
      split atoms group 0[75]=150
      split atoms group 0[76]=152
      split atoms group 0[77]=154
      split atoms group 0[78]=156
      split atoms group 0[79]=158
      split atoms group 0[80]=160
      split atoms group 0[81]=162
      split atoms group 0[82]=164
   massw_split1                   = false
   split atoms group 1 (83):
      split atoms group 1[0]=964
      split atoms group 1[1]=966
      split atoms group 1[2]=968
      split atoms group 1[3]=970
      split atoms group 1[4]=972
      split atoms group 1[5]=974
      split atoms group 1[6]=976
      split atoms group 1[7]=978
      split atoms group 1[8]=980
      split atoms group 1[9]=982
      split atoms group 1[10]=984
      split atoms group 1[11]=986
      split atoms group 1[12]=988
      split atoms group 1[13]=990
      split atoms group 1[14]=992
      split atoms group 1[15]=994
      split atoms group 1[16]=996
      split atoms group 1[17]=998
      split atoms group 1[18]=1000
      split atoms group 1[19]=1002
      split atoms group 1[20]=1004
      split atoms group 1[21]=1006
      split atoms group 1[22]=1008
      split atoms group 1[23]=1010
      split atoms group 1[24]=1012
      split atoms group 1[25]=1014
      split atoms group 1[26]=1016
      split atoms group 1[27]=1018
      split atoms group 1[28]=1020
      split atoms group 1[29]=1022
      split atoms group 1[30]=1024
      split atoms group 1[31]=1026
      split atoms group 1[32]=1028
      split atoms group 1[33]=1030
      split atoms group 1[34]=1032
      split atoms group 1[35]=1034
      split atoms group 1[36]=1036
      split atoms group 1[37]=1038
      split atoms group 1[38]=1040
      split atoms group 1[39]=1042
      split atoms group 1[40]=1044
      split atoms group 1[41]=1046
      split atoms group 1[42]=1048
      split atoms group 1[43]=1050
      split atoms group 1[44]=1052
      split atoms group 1[45]=1054
      split atoms group 1[46]=1056
      split atoms group 1[47]=1058
      split atoms group 1[48]=1060
      split atoms group 1[49]=1062
      split atoms group 1[50]=1064
      split atoms group 1[51]=1066
      split atoms group 1[52]=1068
      split atoms group 1[53]=1070
      split atoms group 1[54]=1072
      split atoms group 1[55]=1074
      split atoms group 1[56]=1076
      split atoms group 1[57]=1078
      split atoms group 1[58]=1080
      split atoms group 1[59]=1082
      split atoms group 1[60]=1084
      split atoms group 1[61]=1086
      split atoms group 1[62]=1088
      split atoms group 1[63]=1090
      split atoms group 1[64]=1092
      split atoms group 1[65]=1094
      split atoms group 1[66]=1096
      split atoms group 1[67]=1098
      split atoms group 1[68]=1100
      split atoms group 1[69]=1102
      split atoms group 1[70]=1104
      split atoms group 1[71]=1106
      split atoms group 1[72]=1108
      split atoms group 1[73]=1110
      split atoms group 1[74]=1112
      split atoms group 1[75]=1114
      split atoms group 1[76]=1116
      split atoms group 1[77]=1118
      split atoms group 1[78]=1120
      split atoms group 1[79]=1122
      split atoms group 1[80]=1124
      split atoms group 1[81]=1126
      split atoms group 1[82]=1128
   solvent group SOL (11931):
      solvent group SOL[0,...,11930] = {1890,...,13820}
   ion group NA+ (19):
      ion group NA+[0,...,18] = {926,...,944}
   ion group CL- (19):
      ion group CL-[0,...,18] = {945,...,963}
   cyl0-r                         = 0.9
   cyl0-up                        = 0.75
   cyl0-down                      = 0.75
   cyl1-r                         = 0.9
   cyl1-up                        = 0.75
   cyl1-down                      = 0.75
   coupl-steps                    = 1
   NA+-in-A                       = 19
   CL--in-A                       = 19
   NA+-in-B                       = 0
   CL--in-B                       = 0
   threshold                      = 1
   bulk-offsetA                   = 0
   bulk-offsetB                   = 0
   userint1                       = 0
   userint2                       = 0
   userint3                       = 0
   userint4                       = 0
   userreal1                      = 0
   userreal2                      = 0
   userreal3                      = 0
   userreal4                      = 0
   applied-forces:
     electric-field:
       x:
         E0                       = 0
         omega                    = 0
         t0                       = 0
         sigma                    = 0
       y:
         E0                       = 0
         omega                    = 0
         t0                       = 0
         sigma                    = 0
       z:
         E0                       = 0
         omega                    = 0
         t0                       = 0
         sigma                    = 0
     density-guided-simulation:
       active                     = false
       group                      = protein
       similarity-measure         = inner-product
       atom-spreading-weight      = unity
       force-constant             = 1e+09
       gaussian-transform-spreading-width = 0.2
       gaussian-transform-spreading-range-in-multiples-of-width = 4
       reference-density-filename = reference.mrc
       nst                        = 1
       normalize-densities        = true
       adaptive-force-scaling     = false
       adaptive-force-scaling-time-constant = 4
       shift-vector               = 
       transformation-matrix      = 
     qmmm-cp2k:
       active                     = false
       qmgroup                    = System
       qmmethod                   = PBE
       qmfilenames                = 
       qmcharge                   = 0
       qmmultiplicity             = 1
grpopts:
   nrdf:       27869
   ref-t:         300
   tau-t:         0.5
annealing:          No
annealing-npoints:           0
   acc:	           0           0           0
   nfreeze:           N           N           N
   energygrp-flags[  0]: 0

Changing nstlist from 10 to 40, rlist from 1.014 to 1.121

When checking whether update groups are usable:
  At least one moleculetype does not conform to the requirements for using update groups

GPU direct communication can not be activated because:
  Swap-coords is not supported.

Initializing Domain Decomposition on 1 ranks
Dynamic load balancing: auto
Minimum cell size due to atom displacement: 0.592 nm
Initial maximum distances in bonded interactions:
    two-body bonded interactions: 0.403 nm, Exclusion, atoms 184 187
  multi-body bonded interactions: 0.403 nm, Ryckaert-Bell., atoms 184 187
Minimum cell size due to bonded interactions: 0.443 nm
Maximum distance for 3 constraints, at 120 deg. angles, all-trans: 0.459 nm
Estimated maximum distance required for P-LINCS: 0.459 nm
Using 0 separate PME ranks because: there are too few total ranks for efficient splitting
Optimizing the DD grid for 1 cells with a minimum initial size of 0.592 nm
The maximum allowed number of cells is: X 16 Y 7 Z 7
Domain decomposition grid 1 x 1 x 1, separate PME ranks 0
PME domain decomposition: 1 x 1 x 1

The initial number of communication pulses is:
The initial domain decomposition cell size is:

The maximum allowed distance for atoms involved in interactions is:
                 non-bonded interactions           1.121 nm
(the following are initial values, they could change due to box deformation)
            two-body bonded interactions  (-rdd)   1.121 nm
          multi-body bonded interactions  (-rdd)   1.121 nm
  atoms separated by up to 3 constraints  (-rcon)  4.317 nm

When dynamic load balancing gets turned on, these settings will change to:
The maximum number of communication pulses is:
The minimum size for domain decomposition cells is 1.121 nm
The requested allowed shrink of DD cells (option -dds) is: 0.80
The allowed shrink of domain decomposition cells is:
The maximum allowed distance for atoms involved in interactions is:
                 non-bonded interactions           1.121 nm
            two-body bonded interactions  (-rdd)   1.121 nm
          multi-body bonded interactions  (-rdd)   1.121 nm
  atoms separated by up to 3 constraints  (-rcon)  1.121 nm

NOTE: GROMACS was compiled without OpenMP and (thread-)MPI support, can only use a single CPU core

Pinning threads with an auto-selected logical cpu stride of 2
System total charge: 0.000
Will do PME sum in reciprocal space for electrostatic interactions.

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
U. Essmann, L. Perera, M. L. Berkowitz, T. Darden, H. Lee and L. G. Pedersen 
A smooth particle mesh Ewald method
J. Chem. Phys. 103 (1995) pp. 8577-8592
-------- -------- --- Thank You --- -------- --------

Using a Gaussian width (1/beta) of 0.320163 nm for Ewald
Potential shift: LJ r^-12: -1.000e+00 r^-6: -1.000e+00, Ewald -1.000e-05
Initialized non-bonded Coulomb Ewald tables, spacing: 9.33e-04 size: 1073

Long Range LJ corr.: <C6> 6.7707e-04


Using plain C 4x4 nonbonded short-range kernels

WARNING: Using the slow plain C kernels. This should
not happen during routine usage on common platforms.

Using a dual 4x4 pair-list setup updated with dynamic pruning:
  outer list: updated every 40 steps, buffer 0.121 nm, rlist 1.121 nm
  inner list: updated every  8 steps, buffer 0.003 nm, rlist 1.003 nm
At tolerance 0.005 kJ/mol/ps per atom, equivalent classical 1x1 list would be:
  outer list: updated every 40 steps, buffer 0.266 nm, rlist 1.266 nm
  inner list: updated every  8 steps, buffer 0.058 nm, rlist 1.058 nm

Using Geometric Lennard-Jones combination rule
Removing pbc first time

Linking all bonded interactions to atoms


Initializing ion/water position exchanges

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
C. Kutzner, H. Grubmuller, B. L. de Groot, and U. Zachariae
Computational Electrophysiology: The Molecular Dynamics of Ion Channel
Permeation and Selectivity in Atomistic Detail
Biophys. J. 101 (2011) pp. 809-817
-------- -------- --- Thank You --- -------- --------


Initializing Parallel LINear Constraint Solver

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
B. Hess
P-LINCS: A Parallel Linear Constraint Solver for molecular simulation
J. Chem. Theory Comput. 4 (2008) pp. 116-122
-------- -------- --- Thank You --- -------- --------

The number of constraints is 1660
There are constraints between atoms in different decomposition domains,
will communicate selected coordinates each lincs iteration

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
S. Miyamoto and P. A. Kollman
SETTLE: An Analytical Version of the SHAKE and RATTLE Algorithms for Rigid
Water Models
J. Comp. Chem. 13 (1992) pp. 952-962
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
H. J. C. Berendsen, J. P. M. Postma, A. DiNola and J. R. Haak
Molecular dynamics with coupling to an external bath
J. Chem. Phys. 81 (1984) pp. 3684-3690
-------- -------- --- Thank You --- -------- --------

There are: 13821 Atoms

Atom distribution over 1 domains: av 13821 stddev 0 min 13821 max 13821

Constraining the starting coordinates (step 0)

Constraining the coordinates at t0-dt (step 0)
Center of mass motion removal mode is Linear
We have the following groups for center of mass motion removal:
  0:  System
RMS relative constraint deviation after constraining: 6.69e-05
Initial temperature: 292.796 K

Started mdrun on rank 0 Wed Jun 15 12:10:10 2022

           Step           Time
              0        0.00000

   Energies (kJ/mol)
          Angle Ryckaert-Bell.        LJ (SR)  Disper. corr.   Coulomb (SR)
    1.49528e+03    2.24982e+03    2.71811e+04   -3.00777e+03   -2.13595e+05
   Coul. recip. Position Rest.      Potential    Kinetic En.   Total Energy
    1.49658e+03    7.11929e-03   -1.84180e+05    3.40032e+04   -1.50176e+05
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)   Constr. rmsd
   -1.50182e+05    2.93490e+02    0.00000e+00    3.25683e+01    2.39733e-04

           Step           Time
              1        0.00400

   Energies (kJ/mol)
          Angle Ryckaert-Bell.        LJ (SR)  Disper. corr.   Coulomb (SR)
    1.50205e+03    2.24615e+03    2.71455e+04   -3.00797e+03   -2.13810e+05
   Coul. recip. Position Rest.      Potential    Kinetic En.   Total Energy
    1.50784e+03    5.01992e-01   -1.84415e+05    3.42068e+04   -1.50209e+05
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)   Constr. rmsd
   -1.50219e+05    2.95248e+02    0.00000e+00   -2.16168e+02    3.56537e-04

Atom distribution over 1 domains: av 13821 stddev 0 min 13821 max 13821
           Step           Time
              2        0.00800

Writing checkpoint, step 2 at Wed Jun 15 12:10:12 2022


   Energies (kJ/mol)
          Angle Ryckaert-Bell.        LJ (SR)  Disper. corr.   Coulomb (SR)
    1.47884e+03    2.23957e+03    3.04686e+04   -3.00797e+03   -2.05699e+05
   Coul. recip. Position Rest.      Potential    Kinetic En.   Total Energy
    7.55406e+03    1.97174e+00   -1.66964e+05    3.53566e+04   -1.31607e+05
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)   Constr. rmsd
   -1.31621e+05    3.05172e+02    0.00000e+00    2.07736e+03    3.88085e-04


Energy conservation over simulation part #1 of length 0.008 ps, time 0 to 0.008 ps
  Conserved energy drift: 1.68e+02 kJ/mol/ps per atom


	<======  ###############  ==>
	<====  A V E R A G E S  ====>
	<==  ###############  ======>

	Statistics over 3 steps using 3 frames

   Energies (kJ/mol)
          Angle Ryckaert-Bell.        LJ (SR)  Disper. corr.   Coulomb (SR)
    1.49206e+03    2.24518e+03    2.82651e+04   -3.00791e+03   -2.11034e+05
   Coul. recip. Position Rest.      Potential    Kinetic En.   Total Energy
    3.51949e+03    8.26949e-01   -1.78520e+05    3.45222e+04   -1.43997e+05
  Conserved En.    Temperature Pres. DC (bar) Pressure (bar)   Constr. rmsd
   -1.44007e+05    2.97970e+02    0.00000e+00    6.31255e+02    0.00000e+00

          Box-X          Box-Y          Box-Z
    9.64840e+00    4.31730e+00    4.31711e+00

   Total Virial (kJ/mol)
    6.88241e+03    3.90996e+02   -1.23603e+03
    3.90927e+02    7.48440e+03   -9.39931e+02
   -1.23594e+03   -9.39915e+02    9.90127e+03

   Pressure (bar)
    8.83639e+02   -7.12087e+01    2.03074e+02
   -7.11958e+01    6.90818e+02    1.81147e+02
    2.03058e+02    1.81144e+02    3.19307e+02


	M E G A - F L O P S   A C C O U N T I N G

 NB=Group-cutoff nonbonded kernels    NxN=N-by-N cluster Verlet kernels
 RF=Reaction-Field  VdW=Van der Waals  QSTab=quadratic-spline table
 W3=SPC/TIP3p  W4=TIP4p (single or pairs)
 V&F=Potential and force  V=Potential only  F=Force only

 Computing:                               M-Number         M-Flops  % Flops
-----------------------------------------------------------------------------
 Pair Search distance check               8.117386          73.056     6.6
 NxN QSTab Elec. + LJ [V&F]               9.254368         546.008    49.3
 NxN LJ [V&F]                             1.204272          51.784     4.7
 NxN QSTab Elec. [V&F]                    8.301344         340.355    30.7
 Calc Weights                             0.124389           4.478     0.4
 Spread Q Bspline                         2.653632           5.307     0.5
 Gather F Bspline                         2.653632          15.922     1.4
 3D-FFT                                   7.153056          57.224     5.2
 Solve PME                                0.006912           0.442     0.0
 Reset In Box                             0.027642           0.083     0.0
 CG-CoM                                   0.055284           0.166     0.0
 Angles                                   0.003420           0.575     0.1
 RB-Dihedrals                             0.002850           0.704     0.1
 Pos. Restr.                              0.000996           0.050     0.0
 Virial                                   0.041598           0.749     0.1
 Stop-CM                                  0.027642           0.276     0.0
 P-Coupling                               0.013821           0.083     0.0
 Calc-Ekin                                0.055284           1.493     0.1
 Lincs                                    0.008300           0.498     0.0
 Lincs-Mat                                0.058720           0.235     0.0
 Constraint-V                             0.061004           0.549     0.0
 Constraint-Vir                           0.040773           0.979     0.1
 Settle                                   0.019885           7.357     0.7
-----------------------------------------------------------------------------
 Total                                                    1108.373   100.0
-----------------------------------------------------------------------------


    D O M A I N   D E C O M P O S I T I O N   S T A T I S T I C S

 av. #atoms communicated per step for force:  2 x 0.0
 av. #atoms communicated per step for LINCS:  2 x 0.0


     R E A L   C Y C L E   A N D   T I M E   A C C O U N T I N G

On 1 MPI rank

 Computing:          Num   Num      Call    Wall time         Giga-Cycles
                     Ranks Threads  Count      (s)         total sum    %
-----------------------------------------------------------------------------
 Domain decomp.         1    1          3       0.122          0.402   6.0
 Neighbor search        1    1          2       0.323          1.067  15.8
 Force                  1    1          3       1.126          3.716  55.2
 PME mesh               1    1          3       0.087          0.287   4.3
 NB X/F buffer ops.     1    1          4       0.001          0.003   0.0
 Write traj.            1    1          3       0.339          1.119  16.6
 Update                 1    1          3       0.004          0.013   0.2
 Constraints            1    1          5       0.025          0.083   1.2
 Position swapping      1    1          1       0.001          0.004   0.1
 Rest                                           0.014          0.045   0.7
-----------------------------------------------------------------------------
 Total                                          2.042          6.738 100.0
-----------------------------------------------------------------------------
 Breakdown of PME mesh computation
-----------------------------------------------------------------------------
 PME spread             1    1          3       0.019          0.062   0.9
 PME gather             1    1          3       0.026          0.086   1.3
 PME 3D-FFT             1    1          6       0.027          0.088   1.3
 PME solve Elec         1    1          3       0.015          0.050   0.7
-----------------------------------------------------------------------------

NOTE: 6 % of the run time was spent in domain decomposition,
      16 % of the run time was spent in pair search,
      you might want to increase nstlist (this has no effect on accuracy)

               Core t (s)   Wall t (s)        (%)
       Time:        2.041        2.042      100.0
                 (ns/day)    (hour/ns)
Performance:        0.508       47.267
Finished mdrun on rank 0 Wed Jun 15 12:10:12 2022

